//
//  TESegmentedControl.m
//  TESegmentedControl
//
//  The MIT License (MIT)
//  Copyright (c) 2016 Tim Ellis
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included
//  in all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "TESegmentedControl.h"
#import "UIView+Constraints.h"

static const NSTimeInterval TESegmentedControlAnimationDuration = 0.1;

@interface TESegmentedControl ()

@property (nonatomic, nonnull) NSMutableArray <UIButton *>* buttons;
@property (nonatomic, nonnull) NSMutableArray <TESegment *>* items;
@property (nonatomic, nonnull) UIView *selectedSegmentIndicator;

@property (nonatomic, readwrite) NSInteger lastTappedIndex;
@property (nonatomic, readwrite, setter=setSelectedIndex:) NSInteger selectedIndex;

@property (nonatomic, nullable, getter=getSelectedButton) UIButton *selectedButton;

@property (nonatomic, nonnull) NSLayoutConstraint *indicatorXConstraint;
@property (nonatomic, nonnull) NSLayoutConstraint *indicatorWidthConstraint;

@property (nonatomic, nonnull) UIButton *placeholderButton;
@property (nonatomic, nonnull) NSArray *placeholderButtonConstraints;
@property (nonatomic, nonnull) NSArray *placeholderButtonConstraintsHide;
@end

@implementation TESegmentedControl

#pragma mark - Getters

- (NSInteger)getNumberOfSegments {
	return self.buttons.count;
}

- (nonnull UIButton *)getSelectedButtonForConstraint {
	UIButton *button = [self getSelectedButton];
	[self layoutPlaceholderButton];
	if (button == nil) {
		[self layoutPlaceholderButton];
		return self.placeholderButton;
	}
	return button;
}

- (nullable UIButton *)getSelectedButton {
	if (self.selectedIndex >= self.buttons.count || self.selectedIndex < 0 || self.selectedIndex >= self.items.count) {
		return nil;
	}
	return [self.buttons objectAtIndex:self.selectedIndex];
}

#pragma mark - Setters

- (void)setFont:(UIFont *)font {
	_font = font;
	for (UIButton *button in self.buttons) {
		button.titleLabel.font = font;
	}
}

- (void)setSegmentIndicatorColor:(UIColor *)segmentIndicatorColor {
	_segmentIndicatorColor = segmentIndicatorColor;
	self.selectedSegmentIndicator.backgroundColor = segmentIndicatorColor;
}

- (void)setSegmentIndicatorThickness:(CGFloat)segmentIndicatorThickness {
	_segmentIndicatorThickness = segmentIndicatorThickness;
}

- (void)setSegmentTextColor:(UIColor *)segmentTextColor {
	_segmentTextColor = segmentTextColor;
	for (UIButton *button in self.buttons) {
		[button setTitleColor:segmentTextColor forState:UIControlStateNormal];
	}
}

- (void)setSegmentSelectedTextColor:(UIColor *)segmentSelectedTextColor {
	_segmentSelectedTextColor = segmentSelectedTextColor;
	for (UIButton *button in self.buttons) {
		[button setTitleColor:segmentSelectedTextColor forState:UIControlStateSelected];
	}
}

- (void)setSegmentDisabledTextColor:(UIColor *)segmentDisabledTextColor {
	_segmentDisabledTextColor = segmentDisabledTextColor;
	for (UIButton *button in self.buttons) {
		[button setTitleColor:segmentDisabledTextColor forState:UIControlStateDisabled];
	}
}

- (void)setSelectedIndex:(NSInteger)selectedIndex {
	for (UIButton *button in self.buttons) {
		button.selected = NO;
	}
	if (selectedIndex >= self.buttons.count || selectedIndex < 0) {
		return;
	}
	[self.buttons objectAtIndex:selectedIndex].selected = YES;
	_selectedIndex = selectedIndex;
	[self layoutIndicator];
}

#pragma mark -
- (void)updateIndicatorConstraints {
	if (self.buttons.count == 0) {
		return;
	}
	if (self.indicatorXConstraint) {
		[self removeConstraint:self.indicatorXConstraint];
	}
	NSLayoutConstraint *xPosConstraint = [NSLayoutConstraint constraintWithItem:self.selectedSegmentIndicator
																	  attribute:NSLayoutAttributeLeft
																	  relatedBy:NSLayoutRelationEqual
																		 toItem:self
																	  attribute:NSLayoutAttributeLeft
																	 multiplier:1.0f
																	   constant:0.0f
										  ];
	self.indicatorXConstraint = xPosConstraint;
	[self addConstraint:xPosConstraint];
	
	if (self.indicatorWidthConstraint ) {
		[self removeConstraint:self.indicatorWidthConstraint];
	}
	NSLayoutConstraint *widthConstraint = [NSLayoutConstraint constraintWithItem:self.selectedSegmentIndicator
																	   attribute:NSLayoutAttributeWidth
																	   relatedBy:NSLayoutRelationEqual
																		  toItem:[self getSelectedButton]
																	   attribute:NSLayoutAttributeWidth
																	  multiplier:1.0f
																		constant:0.0f
										   ];
	self.indicatorWidthConstraint = widthConstraint;
	[self addConstraint:widthConstraint];
}

- (void)updateIndicatorState {
	static BOOL oldState = YES;
	self.selectedSegmentIndicator.hidden = self.buttons.count == 0;
	if (oldState != self.selectedSegmentIndicator.hidden) {
		oldState = self.selectedSegmentIndicator.hidden;
		[self updateIndicatorConstraints];
	}
}

- (void)setupIndicator {
	if (self.selectedSegmentIndicator != nil) {
		// Don't create more than 1 instance
		return;
	}
	// Create seg
	UIView *segView = [UIView new];
	self.selectedSegmentIndicator = segView;
	// Fix autolayout
	segView.translatesAutoresizingMaskIntoConstraints = NO;
	// Set background color
	segView.backgroundColor = (self.segmentIndicatorColor) ? self.segmentIndicatorColor : self.tintColor;
	// Add view
	[self addSubview:segView];
	// Add constraints
	NSLayoutConstraint *yPosConstraint = [NSLayoutConstraint constraintWithItem:segView
																	  attribute:NSLayoutAttributeBottom
																	  relatedBy:NSLayoutRelationEqual
																		 toItem:self
																	  attribute:NSLayoutAttributeBottom
																	 multiplier:1.0f
																	   constant:0.0f
										  ];
	[self addConstraint:yPosConstraint];
	
	NSLayoutConstraint *heightConstraint = [NSLayoutConstraint constraintWithItem:segView
																		attribute:NSLayoutAttributeHeight
																		relatedBy:NSLayoutRelationEqual
																		   toItem:nil
																		attribute:NSLayoutAttributeNotAnAttribute
																	   multiplier:1.0f
																		 constant:self.segmentIndicatorThickness
											];
	[self addConstraint:heightConstraint];
	[self updateIndicatorState];
}

- (void)setupPlaceholderButtonConstraints {
	NSArray *xConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(-100)-[dummy(10)]" options:0 metrics:0 views:@{@"dummy":self.placeholderButton}];
	NSArray *yConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(-100)-[dummy(10)]" options:0 metrics:0 views:@{@"dummy":self.placeholderButton}];
	self.placeholderButtonConstraintsHide = [xConstraints arrayByAddingObjectsFromArray:yConstraints];
	xConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[dummy]|" options:0 metrics:nil views:@{@"dummy":self.placeholderButton}];
	yConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dummy]|" options:0 metrics:nil views:@{@"dummy":self.placeholderButton}];
	self.placeholderButtonConstraints = [xConstraints arrayByAddingObjectsFromArray:yConstraints];
}

#pragma mark -

/**
 *    Common set up actions that all init events call
 */
- (void)setupWithItems:(nullable NSArray<TESegment *> *)items {
	self.items = [NSMutableArray new];
	if (self.buttons) {
		for (UIButton *button in self.buttons) {
			[button removeFromSuperview];
		}
		[self.buttons removeAllObjects];
	}
	else {
		self.buttons = [NSMutableArray new];
	}
	if (!self.placeholderText) {
		self.placeholderText = @"No buttons configured";
	}

	if (!self.placeholderButton) {
		self.placeholderButton = [UIButton buttonWithType:UIButtonTypeCustom];
		self.placeholderButton.translatesAutoresizingMaskIntoConstraints = NO;
		self.placeholderButton.enabled = NO;
		[self addSubview:self.placeholderButton];
		[self setupPlaceholderButtonConstraints];
		[self layoutPlaceholderButton];
		[self addConstraints:self.placeholderButtonConstraints];
		[self.placeholderButton setTitle:self.placeholderText forState:UIControlStateNormal];
		
	}
	
	if (!self.font) {
		self.font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
	}
	if (!self.segmentIndicatorColor) {
		self.segmentIndicatorColor = (self.tintColor) ? self.tintColor : [UIColor blueColor];
	}
	if (!self.segmentTextColor) {
		self.segmentTextColor = [UIColor grayColor];
	}
	if (!self.segmentSelectedTextColor) {
		self.segmentSelectedTextColor = [UIColor blackColor];
	}
	if (!self.segmentDisabledTextColor) {
		self.segmentDisabledTextColor = [UIColor lightGrayColor];
	}
	if (self.segmentIndicatorThickness <= 0) {
		self.segmentIndicatorThickness = 3;
	}
	[self setBackgroundColor:[UIColor colorWithWhite:0.9 alpha:1.0]];
	self.lastTappedIndex = -1;
	self.selectedIndex = 0;
	self.animatable = YES;
	[self appendSegments:items animated:NO];
	[self setupIndicator];
	[self setNeedsLayout];
}

- (instancetype)init {
	if (self = [super init]) {
		[self setupWithItems:nil];
	}
	return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
	if (self = [super initWithCoder:aDecoder]) {
		[self setupWithItems:nil];
	}
	return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
	if (self = [super initWithFrame:frame]) {
		[self setupWithItems:nil];
	}
	return self;
}

- (instancetype)initWithItems:(NSArray<TESegment *> *)items {
	if (self = [super init]) {
		[self setupWithItems:items];
	}
	return self;
}

- (BOOL)validateButton:(UIButton *)button {
	if (button.tag >= self.buttons.count || button.tag < 0 || button.tag >= self.items.count) {
		return false;
	}
	return [self.buttons objectAtIndex:button.tag] == button;
}

- (void)regenerateButtonTags {
	NSInteger idx = 0;
	for (UIButton *button in self.buttons) {
		button.tag = idx++;
	}
	[self setSelectedIndex:self.selectedIndex];
}

#pragma mark - Managing Segments
- (void)uiAddSegments:(NSArray<TESegment *> *)segments atIndex:(NSInteger)idx animated:(BOOL)animated {
	if (self.items.count > 0) {
		if (idx < 0) {
			idx = self.items.count;
		}
		if (idx <= self.selectedIndex) {
			// Inserting a segment before the selected index. Increment the selected index to keep the old selection
			self.selectedIndex += [segments count];
		}
		if (idx > self.items.count) {
			idx = self.items.count;
		}
	}
	else {
		idx = 0;
	}
	
	for (TESegment *segment in segments) {
		[self.items insertObject:segment atIndex:idx];
		UIButton *button = [self generateButtonForSegment:segment atIndex:idx];
		button.translatesAutoresizingMaskIntoConstraints = NO;
		[self addSubview:button];
		[self.buttons insertObject:button atIndex:idx];
		idx++;
	}
	
	[self regenerateButtonTags];
	[self layoutPlaceholderButton];
	[UIView animateWithDuration:[self getAnimationTime:animated] animations:^{
		[self layoutButtons];
	}];
}

- (void)insertSegment:(TESegment *)segment atIndex:(NSInteger)idx animated:(BOOL)animated {
	[self uiAddSegments:@[segment] atIndex:idx animated:animated];
}

- (void)insertSegments:(NSArray<TESegment *> *)segments atIndex:(NSInteger)idx animated:(BOOL)animated {
	[self uiAddSegments:segments atIndex:idx animated:animated];
}

- (void)appendSegment:(TESegment *)segment animated:(BOOL)animated {
	[self insertSegment:segment atIndex:-1 animated:animated];
}

- (void)appendSegments:(NSArray<TESegment *> *)segments animated:(BOOL)animated {
	[self insertSegments:segments atIndex:-1 animated:animated];
}

- (void)removeSegmentAtIndex:(NSInteger)idx animated:(BOOL)animated {
	if (self.items.count == 0) {
		// If there are no items then abort
		return;
	}
	TESegment *segment = nil;
	UIButton *button = nil;
	if (idx < 0) {
		segment = [self.items lastObject];
		button = [self.buttons lastObject];
	}
	else {
		if (idx >= self.items.count) {
			idx = self.items.count-1;
		}
		segment = [self.items objectAtIndex:idx];
		button = [self.buttons objectAtIndex:idx];
	}
	if (idx <= self.selectedIndex) {
		// Removing a segment before the selected index. Decrement the selected index to keep the old selection
		self.selectedIndex--;
	}
	[UIView animateWithDuration:[self getAnimationTime:animated] animations:^{
		if (segment) {
			[self.items removeObject:segment];
		}
		if (button) {
			[button removeFromSuperview];
			[self.buttons removeObject:button];
			[self regenerateButtonTags];
			[self layoutPlaceholderButton];
			[self layoutButtons];
		}
	}];
}

- (void)removeAllSegments {
	if (self.items.count == 0) {
		// If there are no items then abort
		return;
	}
	[self.items removeAllObjects];
	for (UIButton *button in self.buttons) {
		[button removeFromSuperview];
	}
	self.selectedIndex = 0;
	[self.buttons removeAllObjects];
	[self regenerateButtonTags];
	[self layoutPlaceholderButton];
	[self layoutButtons];
}

#pragma mark - Managing Segment Interactions
- (NSTimeInterval)getAnimationTime:(BOOL)animated {
	return animated ? TESegmentedControlAnimationDuration : 0.0;
}

- (void)performSegmentActionWithSegment:(TESegment *)segment {
	if (segment.actionBlock != nil) {
		segment.actionBlock();
	}
}

- (void)setSelectedSegmentIndex:(NSInteger)idx animated:(BOOL)animated {
	[self layoutIfNeeded];
	[UIView animateWithDuration:[self getAnimationTime:animated] animations:^{
		self.selectedIndex = idx;
		[self layoutIfNeeded];
	} completion:^(BOOL finished) {
		if (finished) {
			if (self.selectedIndex < self.items.count && self.selectedIndex >= 0) {
				[self performSegmentActionWithSegment:[self.items objectAtIndex:self.selectedIndex]];
			}
			[self sendActionsForControlEvents:UIControlEventValueChanged];
		}
	}];
}

#pragma mark - Button Interactions
- (void)buttonTappableTapped:(UIButton *)sender {
	if (![self validateButton:sender]) {
		return;
	}
	self.lastTappedIndex = sender.tag;
	if (self.lastTappedIndex < self.items.count && self.lastTappedIndex >= 0) {
		[self performSegmentActionWithSegment:[self.items objectAtIndex:self.lastTappedIndex]];
	}
}

- (void)buttonSelectableTapped:(UIButton *)sender {
	if (![self validateButton:sender]) {
		return;
	}
	self.lastTappedIndex = sender.tag;
	[self setSelectedSegmentIndex:sender.tag animated:self.animatable];
}

#pragma mark - Generator methods
- (UIButton *)generateButtonForSegment:(TESegment *)segment atIndex:(NSInteger)idx {
	UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
	button.titleLabel.font = self.font;
	[button setTitleColor:self.segmentTextColor forState:UIControlStateNormal];
	[button setTitleColor:self.segmentSelectedTextColor	forState:UIControlStateSelected];
	[button setTitleColor:self.segmentSelectedTextColor forState:UIControlStateHighlighted];
	[button setTitleColor:self.segmentSelectedTextColor forState:UIControlStateHighlighted];
	[button setTitleColor:self.segmentDisabledTextColor forState:UIControlStateDisabled];
	[button setTitle:segment.title forState:UIControlStateNormal];
	button.enabled = segment.style != TESegmentStyleDisabled;
	button.tag = idx;
	if (button.enabled) {
		if (segment.style == TESegmentStyleSelectable) {
			[button addTarget:self action:@selector(buttonSelectableTapped:) forControlEvents:UIControlEventTouchUpInside];
		}
		else { // assume TESegmentStyleTappable
			[button addTarget:self action:@selector(buttonTappableTapped:) forControlEvents:UIControlEventTouchUpInside];
		}
	}
	return button;
}

#pragma UI Update methods
- (void)layoutButtons {
	if (self.buttons.count <= 0 || self.buttons.firstObject == nil) {
		return;
	}
	for (UIButton *button in self.buttons) {
		[button removeMyConstraintsNotRelatedTo:self.selectedSegmentIndicator];
	}
	UIButton *button = self.buttons.firstObject;
	// Generic Y constraints for all buttons
	NSArray *yConstraints = [NSLayoutConstraint constraintsWithVisualFormat: @"V:|[button]|"
																	options: 0
																	metrics: nil
																	  views: @{
																			   @"button": button
																			   }
							 ];
	
	NSMutableString *xVisualFormat = [NSMutableString stringWithString:@"H:|"];
	NSMutableDictionary <NSString*, UIButton*>*xViews = [NSMutableDictionary new];

	NSString *prevButtonName = nil;
	NSInteger buttonIdx = 0;
	for (UIButton *currentButton in self.buttons) {
		NSString *buttonName = [NSString stringWithFormat:@"button%ld", (long)buttonIdx];
		[xViews setObject:currentButton forKey:buttonName];
		
		if (prevButtonName != nil) {
			[xVisualFormat appendFormat:@"[%@(==%@)]", buttonName, prevButtonName];
		}
		else {
			[xVisualFormat appendFormat:@"[%@]", buttonName];
		}
		
		prevButtonName = buttonName;
		buttonIdx++;
	}
	
	[xVisualFormat appendString:@"|"];
	NSArray *xConstraints = [NSLayoutConstraint constraintsWithVisualFormat: xVisualFormat
																	options: NSLayoutFormatAlignAllTop | NSLayoutFormatAlignAllBottom
																	metrics: nil
																	  views: xViews
							 ];
	
	[NSLayoutConstraint activateConstraints:xConstraints];
	[NSLayoutConstraint activateConstraints:yConstraints];
}

- (void)layoutIndicator {
	UIButton *selectedButton = self.selectedButton;
	[self updateIndicatorState];
	if (selectedButton) {
		self.indicatorXConstraint.constant = selectedButton.frame.origin.x;
	}
}

- (void)layoutPlaceholderButton {
	if (self.buttons.count) {
		self.placeholderButton.hidden = YES;
	}
	else {
		self.placeholderButton.hidden = NO;
	}
}

- (void)layoutSubviews {
	[super layoutSubviews];
	[self layoutIndicator];
	[self layoutPlaceholderButton];
}

#pragma mark - IB Dummy Data
- (void)prepareForInterfaceBuilder {
	[super prepareForInterfaceBuilder];
	[self setupWithItems:@[
					   [[TESegment alloc] initWithTitle:@"Item 1" image:nil style:TESegmentStyleSelectable],
					   [[TESegment alloc] initWithTitle:@"Item 2" image:nil style:TESegmentStyleSelectable],
					   [[TESegment alloc] initWithTitle:@"Item 3" image:nil style:TESegmentStyleSelectable],
					   ]];
}

@end
