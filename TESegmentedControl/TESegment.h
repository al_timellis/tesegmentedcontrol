//
//  TESegment.h
//  TESegmentedControl
//
//  The MIT License (MIT)
//  Copyright (c) 2016 Tim Ellis
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included
//  in all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
	/// The item can be selected (with line underneath)
	TESegmentStyleSelectable,
	/// The item can be selected but the line will remain on the old one
	TESegmentStyleTappable,
	/// The item cannot be selected
	TESegmentStyleDisabled,
} TESegmentStyle;

@interface TESegment : NSObject

/// The title of the segment (can be nil)
@property (nonatomic, nullable) NSString *title;

/// The image to show (can be nil)
@property (nonatomic, nullable) UIImage *image;

@property (nonatomic, copy, nullable) void (^actionBlock)(void);

/**
 *    The style of the segment
 *
 *    Defaults to TESegmentStyleSelectable
 */
@property (nonatomic) TESegmentStyle style;

- (nonnull instancetype)initWithTitle:(nullable NSString *)title
								image:(nullable UIImage *)image
								style:(TESegmentStyle)style;

- (nonnull instancetype)initWithTitle:(nullable NSString *)title;

- (nonnull instancetype)initWithImage:(nullable UIImage *)image;

// Helper methods to speed up time
+ (nonnull instancetype)segmentWithTitle:(nullable NSString *)title
								   image:(nullable UIImage *)image
								   style:(TESegmentStyle)style;

+ (nonnull instancetype)segmentWithTitle:(nullable NSString *)title;

+ (nonnull instancetype)segmentWithImage:(nullable UIImage *)image;


@end
