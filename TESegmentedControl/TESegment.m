//
//  TESegment.m
//  TESegmentedControl
//
//  The MIT License (MIT)
//  Copyright (c) 2016 Tim Ellis
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included
//  in all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "TESegment.h"

@implementation TESegment

- (instancetype)initWithTitle:(NSString *)title image:(UIImage *)image style:(TESegmentStyle)style{
	if (self = [super init]) {
		self.title = title;
		self.image = image;
		self.style = style;
	}
	return self;
}

- (instancetype)initWithTitle:(NSString *)title {
	return [self initWithTitle:title image:nil style:TESegmentStyleSelectable];
}

- (instancetype)initWithImage:(UIImage *)image {
	return [self initWithTitle:nil image:image style:TESegmentStyleSelectable];
}

// Class functions
+ (instancetype)segmentWithTitle:(NSString *)title image:(UIImage *)image style:(TESegmentStyle)style {
	return [[self alloc] initWithTitle:title
								 image:image
								 style:style
			];
}

+ (instancetype)segmentWithTitle:(NSString *)title {
	return [[self alloc] initWithTitle:title];
}

+ (instancetype)segmentWithImage:(UIImage *)image {
	return [[self alloc] initWithImage:image];
}

@end
