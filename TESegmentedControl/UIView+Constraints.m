//
//  UIView+Constraints.m
//  TESegmentedControl
//
//  The MIT License (MIT)
//  Copyright (c) 2016 Tim Ellis
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included
//  in all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "UIView+Constraints.h"

@implementation UIView (Constraints)

- (void)removeMyConstraintsAxis:(UILayoutConstraintAxis)axis {
	UIView *superview = self.superview;
	while (superview != nil) {
		NSArray *constraints = [self.superview constraintsAffectingLayoutForAxis:axis];
		if (constraints.count) {
			for (NSLayoutConstraint *constraint in constraints) {
				if (constraint.firstItem == self || constraint.secondItem == self) {
					[superview removeConstraint:constraint];
				}
			}
		}
		superview = superview.superview;
	}
}

- (void)removeMyConstraintsNotRelatedTo:(UIView *)view {
	UIView *superview = self.superview;
	while (superview != nil) {
		NSArray *constraints = [self.superview constraints];
		if (constraints.count) {
			for (NSLayoutConstraint *constraint in constraints) {
				if (constraint.firstItem == self || constraint.secondItem == self) {
					if (
						view == nil ||
						(constraint.firstItem != view && constraint.secondItem != view)
					) {
						[superview removeConstraint:constraint];
					}
				}
			}
		}
		superview = superview.superview;
	}
}

- (void)removeMyConstraints {
	[self removeMyConstraintsNotRelatedTo:nil];
}

- (void)removeHorizontalConstraints {
	[self removeMyConstraintsAxis:UILayoutConstraintAxisHorizontal];
}

- (void)removeVerticalConstraints {
	[self removeMyConstraintsAxis:UILayoutConstraintAxisVertical];
}

@end
