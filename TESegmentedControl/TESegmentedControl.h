//
//  TESegmentedControl.h
//  TESegmentedControl
//
//  The MIT License (MIT)
//  Copyright (c) 2016 Tim Ellis
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included
//  in all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import <UIKit/UIKit.h>
#import "TESegment.h"

IB_DESIGNABLE
@interface TESegmentedControl : UIControl

@property (nonatomic, nonnull) IBInspectable UIFont *font;

@property (nonatomic, nonnull) IBInspectable UIColor *segmentIndicatorColor;
@property (nonatomic) IBInspectable CGFloat segmentIndicatorThickness;

@property (nonatomic, nonnull) IBInspectable UIColor *segmentTextColor;
@property (nonatomic, nonnull) IBInspectable UIColor *segmentSelectedTextColor;
@property (nonatomic, nonnull) IBInspectable UIColor *segmentDisabledTextColor;

@property (nonatomic, nonnull, assign) IBInspectable NSString *placeholderText;

@property (nonatomic) IBInspectable BOOL animatable;

// Getters
@property (nonatomic, readonly) NSInteger lastTappedIndex;
@property (nonatomic, readonly) NSInteger selectedIndex;
@property (nonatomic, readonly, getter=getNumberOfSegments) NSInteger numberOfSegments;

- (nonnull instancetype)initWithItems:(nullable NSArray <TESegment *>*)items;

/**
 *    Inserts a segment into the control at a given index.
 *    Passing `-1` as the index will append it to the end.
 *
 *    NB: If the index is out of range it will be fixed appropriately - although do not rely on it.
 *
 *    @param segment  The segment to append
 *    @param idx      The index to insert the segment at (-1 for append to end)
 *    @param animated `true` if you want it to animate the insert.
 */
- (void)insertSegment:(nonnull TESegment *)segment atIndex:(NSInteger)idx animated:(BOOL)animated;


/**
 *    Inserts an array of segments into the control at a given index.
 *    Passing `-1` as the index will append it to the end.
 *
 *    NB: If the index is out of range it will be fixed appropriately - although do not rely on it.
 *
 *    @param segments The segments to append
 *    @param idx      The index to insert the segment at (-1 for append to end)
 *    @param animated `true` if you want it to animate the insert.
 */
- (void)insertSegments:(nonnull NSArray <TESegment *>*)segments atIndex:(NSInteger)idx animated:(BOOL)animated;

/**
 *    Appends a segment into the control at the end.
 *
 *    @param segment  The segment to append
 *    @param animated `true` if you want it to animate the append.
 */
- (void)appendSegment:(nonnull TESegment *)segment animated:(BOOL)animated;

/**
 *    Appends an array of segments into the control at the end.
 *
 *    @param segments The segments to append
 *    @param animated `true` if you want it to animate the append.
 */
- (void)appendSegments:(nonnull NSArray <TESegment *>*)segments animated:(BOOL)animated;

/**
 *    Removes a segment from the control at the given index.
 *
 *    @param idx      The index of the segment to remove
 *    @param animated `true` if you want it to animate the remove.
 */
- (void)removeSegmentAtIndex:(NSInteger)idx animated:(BOOL)animated;

/**
 *	Removes all segments from the control
 */
- (void)removeAllSegments;

@end
