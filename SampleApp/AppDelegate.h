//
//  AppDelegate.h
//  SampleApp
//
//  Created by Timothy Ellis on 12/05/2016.
//  Copyright © 2016 Tim Ellis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

