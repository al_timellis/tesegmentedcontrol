//
//  ViewController.m
//  SampleApp
//
//  Created by Timothy Ellis on 12/05/2016.
//  Copyright © 2016 Tim Ellis. All rights reserved.
//

#import "ViewController.h"

#import "TESegmentedControl.h"
#import "TESegment.h"

@interface ViewController ()

@property (nonatomic, nonnull) TESegmentedControl *segControl;

@end

@implementation ViewController

- (IBAction)removeTouchUp:(id)sender {
	[self.segControl removeSegmentAtIndex:1 animated:YES];
}

- (IBAction)touchUp:(id)sender {
	static int idx = 1;
	NSString *str1 = [NSString stringWithFormat:@"%d", idx++];
	NSString *str2 = [NSString stringWithFormat:@"%d", idx++];
	TESegment *seg1 = [[TESegment alloc] initWithTitle:str1];
	TESegment *seg2 = [[TESegment alloc] initWithTitle:str2];
	[seg1 setActionBlock:^{
		NSLog(@"tapped in block");
	}];
	[seg2 setActionBlock:^{
		NSLog(@"tapped 2 in block");
	}];
	[self.segControl appendSegments:@[seg1, seg2]
						  animated:true];
}

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	NSArray *items = [NSArray arrayWithObjects:
					  [[TESegment alloc] initWithTitle:@"Sel 1"],
					  [[TESegment alloc] initWithTitle:@"Sel 2"],
					  [[TESegment alloc] initWithTitle:@"Dis 3" image:nil style:TESegmentStyleDisabled],
					  [[TESegment alloc] initWithTitle:@"Sel 4"],
					  [[TESegment alloc] initWithTitle:@"Tap 5" image:nil style:TESegmentStyleTappable],
					  nil
					  ];
	self.segControl = [[TESegmentedControl alloc] initWithItems:items];
	self.segControl.translatesAutoresizingMaskIntoConstraints = NO;
	[self.view addSubview:self.segControl];

	NSArray *hConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[obj]|" options:0 metrics:0 views:@{@"obj":self.segControl}];
	NSArray *vConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[obj(49)]|" options:0 metrics:0 views:@{@"obj":self.segControl}];
	[self.view addConstraints:hConstraints];
	[self.view addConstraints:vConstraints];

}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
