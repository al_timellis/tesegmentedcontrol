Pod::Spec.new do |s|
    s.name				=	'TESegmentedControl'
    s.version			=	'0.2'
    s.license			=	{
        :type => 'MIT',
        :text => <<-LICENSE
            TESegmentedNavigationController
            
            The MIT License (MIT)
            Copyright (c) 2016 Tim Ellis
            
            Permission is hereby granted, free of charge, to any person obtaining a copy
            of this software and associated documentation files (the "Software"), to deal
            in the Software without restriction, including without limitation the rights
            to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
            copies of the Software, and to permit persons to whom the Software is
            furnished to do so, subject to the following conditions:
            
            The above copyright notice and this permission notice shall be included
            in all copies or substantial portions of the Software.
            
            THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
            IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
            FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
            AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
            LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
            OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
            THE SOFTWARE.
        LICENSE
    }
    s.summary			=	'Glorified radio buttons.'
    s.description		=	<<-DESC
    							An interface builder designable tab bar styled view.
                                It has the ability to add and remove segments on the fly.
                                
                                NB: This is the base class and should only be used if you have a complex layout and need direct access to manipulating the TESegmentedControl.
    						DESC

    s.homepage			=	'https://bitbucket.org/al_timellis/tesegmentedcontrol'
    s.authors			=	{
        'Tim Ellis' => 'crazyivan444@gmail.com'
    }
    s.source			=	{
        :git => 'https://bitbucket.org/al_timellis/tesegmentedcontrol.git',
        :tag => '0.2'
    }
    
    s.ios.deployment_target = "8.0"
    
    s.source_files		=	[
        'TESegmentedControl/TESegment.h',
        'TESegmentedControl/TESegment.m',
        'TESegmentedControl/TESegmentedControl.h',
        'TESegmentedControl/TESegmentedControl.m',
        'TESegmentedControl/UIView+Constraints.h',
        'TESegmentedControl/UIView+Constraints.m'
    ]
    s.requires_arc		=	true
    
end