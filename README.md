# TESegmentedControl #

Current Version: **0.1**

Project Status: Beta

---

Licensed under [MIT](https://en.wikipedia.org/wiki/MIT_License)

## Install methods ##

### Cocoapods ###

To install via cocoapods include the following line:

```
pod 'TESegmentedControl'
```

### Manually ###

Download the project and copy the contents of the folder `TESegmentedControl` into your project.

## Issues/Bugs? ##

If you find any issues or bugs please feel free to use the **Issues** tab in the repository to report it.